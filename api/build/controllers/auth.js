'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _auth = require("./../utils/api/auth");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express.default.Router();
/**
 * Login route. Verifies provided username and password. If they are correct
 * the API sets a HttpOnly cookie with Authorization Token for the session lifetime
 */


router.post('/login', function (req, res) {
  pebee.models.User.verifyPassword(req.body).then(function (user) {
    if (user) {
      var userData = user.serialize(),
          token;

      try {
        if (req.cookies['authorizationToken'] && (0, _auth.verifyToken)(req.cookies['authorizationToken'])) {
          token = req.cookies['authorizationToken'];
        } else {
          token = (0, _auth.createToken)({
            id: user.get('id')
          });
        }
      } catch (e) {
        throw e;
      }

      var cookieExpiration = parseInt(process.env.TOKEN_EXPIRATION) * 1000;
      res.cookie('authorizationToken', token, {
        httpOnly: true,
        expires: new Date(Date.now() + cookieExpiration)
      });
      res.send({
        message: _t('Login successful'),
        data: userData
      });
    } else {
      res.status(403).send(pebee.api.responses.loginError());
    }
  }).catch(function (e) {
    res.status(403).send(pebee.api.responses.loginError());
  });
}); // get current logged in user

router.get('/me', function (req, res) {
  res.send(req.user);
}); // update current logged in user

router.put('/me', function (req, res) {
  pebee.models.User.findById(req.user.id).then(function (user) {
    if (user) {
      // prevent updating account category by yourself
      req.body.accountCategory = req.user.accountCategory.id;
      return user.update(req.body).then(function (self) {
        self.reload({
          include: [pebee.models.AccountCategory]
        }).then(function (self) {
          res.send(pebee.api.responses.updated(self.serialize(), _t('pebee.auth.accountSaved')));
        });
      });
    } else {
      res.status(404).send(pebee.api.responses.notFound(_t('pebee.auth.userNotFound'), {}));
    }
  }).catch(function (e) {
    res.status(400).send(pebee.api.responses.modelError(e));
  });
}); // change password of current logged in user

router.put('/me/change-password', function (req, res) {
  pebee.models.User.findById(req.user.id).then(function (user) {
    if (user) {
      return user.changePassword(req.body['password']).then(function (self) {
        res.send({
          statusCode: 200,
          message: _t('pebee.auth.passwordChanged')
        });
      });
    } else {
      res.status(404).send(pebee.api.responses.notFound(_t('pebee.auth.userNotFound'), {}));
    }
  }).catch(function (e) {
    res.status(400).send(pebee.api.responses.modelError(e));
  });
});
var _default = router;
exports.default = _default;