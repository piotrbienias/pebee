'use strict';
/**
 * Get current language from database and put it in the request
 */

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkSpecificPermission = exports.checkPermission = exports.loadLanguage = void 0;

var loadLanguage = function loadLanguage(req, res, next) {
  // set language setting cache time to 24 hours
  var langCacheTime = 1000 * 3600 * 24;
  pebee.models.Option.findById('lang').then(function (language) {
    req.lang = language.get('value');
    memCache.put('lang', req.lang, langCacheTime);
    next();
  });
};
/**
 * Check permissions for basic CRUD operations
 */


exports.loadLanguage = loadLanguage;

var checkPermission = function checkPermission(entityName) {
  return function (req, res, next) {
    if (req.method !== 'OPTIONS') {
      var permissionLabel = pebee.functions.generateCRUDPermissionLabel(entityName, req.method);

      if (pebee.functions.validatePermission(req.user, permissionLabel) || process.env.USE_AUTH === '0') {
        next();
      } else {
        res.status(401).send(pebee.api.responses.notAuthorized());
      }
    } else {
      next();
    }
  };
};
/**
 * Validate user against specific permission
 */


exports.checkPermission = checkPermission;

var checkSpecificPermission = function checkSpecificPermission(permission) {
  return function (req, res, next) {
    if (req.method !== 'OPTIONS') {
      if (pebee.functions.validatePermission(req.user, permission) || process.env.USE_AUTH === '0') {
        next();
      } else {
        res.status(401).send(pebee.api.responses.notAuthorized());
      }
    } else {
      next();
    }
  };
};

exports.checkSpecificPermission = checkSpecificPermission;