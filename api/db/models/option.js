'use strict';

const fs = require('fs');
const path = require('path');
const slugify = require('slugify');


module.exports = (sequelize, DataTypes) => {
    
    const Option = sequelize.define('Option', {
        key: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            primaryKey: true
        },
        displayName: DataTypes.STRING,
        value: DataTypes.STRING,
        availableValues: DataTypes.ARRAY(DataTypes.STRING),
        isProtected: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        hooks: {
            beforeValidate: (option, options) => {
                option.key = slugify(option.displayName).toLowerCase();
            }
        },
        tableName: 'options',
        timestamps: true,
        paranoid: false
    });

    Option.bulkUpdate = function(data) {
        let keys = Object.keys(data);

        let promisesArray = [];
        keys.forEach(key => {
            let promise = Option.findById(key).then(option => {
                if (option) {
                    if (option.key === 'lang') pebee.functions.changeLanguage(data[key]);
                    return option.update({ value: data[key] });
                } else {
                    return Promise.resolve(undefined);
                }
            });
            promisesArray.push(promise);
        });

        return Promise.all(promisesArray);
    }

    Option.prototype.serialize = function() {
        let option = this.toJSON();

        option.displayName = _t(`pebee.options.${option.key}`, option.displayName);

        return option;
    }

    return Option;

};