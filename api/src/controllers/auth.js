'use strict';

import express from 'express';

import { createToken, verifyToken } from './../utils/api/auth';

const router = express.Router();


/**
 * Login route. Verifies provided username and password. If they are correct
 * the API sets a HttpOnly cookie with Authorization Token for the session lifetime
 */
router.post('/login', (req, res) => {

    pebee.models.User.verifyPassword(req.body).then(user => {
        if (user) {
            let userData = user.serialize(),
                token;

            try {
                if (req.cookies['authorizationToken'] && verifyToken(req.cookies['authorizationToken']) ) {
                    token = req.cookies['authorizationToken'];
                } else {
                    token = createToken({ id: user.get('id') });
                }    
            } catch (e) {
                throw e;
            }
                
            let cookieExpiration = parseInt(process.env.TOKEN_EXPIRATION) * 1000;
            res.cookie(
                'authorizationToken',
                token,
                { httpOnly: true, expires:  new Date(Date.now() + cookieExpiration) }
            );

            res.send({
                message: _t('Login successful'),
                data: userData
            });
        } else {
            res.status(403).send(pebee.api.responses.loginError());
        }
    }).catch(e => {
        res.status(403).send(pebee.api.responses.loginError());
    });

});


// get current logged in user
router.get('/me', (req, res) => {

    res.send(req.user);

});


// update current logged in user
router.put('/me', (req, res) => {

    pebee.models.User.findById(req.user.id).then(user => {
        if (user) {
            // prevent updating account category by yourself
            req.body.accountCategory = req.user.accountCategory.id;

            return user.update(req.body).then(self => {
                self.reload({ include: [pebee.models.AccountCategory] }).then(self => {
                    res.send(pebee.api.responses.updated(self.serialize(), _t('pebee.auth.accountSaved')));
                });
            });
        } else {
            res.status(404).send(pebee.api.responses.notFound(_t('pebee.auth.userNotFound'), {}));
        }
    }).catch(e => {
        res.status(400).send(pebee.api.responses.modelError(e));
    });

});


// change password of current logged in user
router.put('/me/change-password', (req, res) => {

    pebee.models.User.findById(req.user.id).then(user => {
        if (user) {
            return user.changePassword(req.body['password']).then(self => {
                res.send({ statusCode: 200, message: _t('pebee.auth.passwordChanged') });
            });
        } else {
            res.status(404).send(pebee.api.responses.notFound(_t('pebee.auth.userNotFound'), {}));
        }
    }).catch(e => {
        res.status(400).send(pebee.api.responses.modelError(e));
    });

});


export default router;