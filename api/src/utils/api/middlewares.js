'use strict';


/**
 * Get current language from database and put it in the request
 */
export const loadLanguage = (req, res, next) => {

    // set language setting cache time to 24 hours
    let langCacheTime = 1000 * 3600 * 24;

    pebee.models.Option.findById('lang').then(language => {
        req.lang = language.get('value');
        memCache.put('lang', req.lang, langCacheTime);

        next();
    });

};


/**
 * Check permissions for basic CRUD operations
 */
export const checkPermission = entityName => {

    return (req, res, next) => {
        if ( req.method !== 'OPTIONS' ) {

            let permissionLabel = pebee.functions.generateCRUDPermissionLabel(entityName, req.method);

            if (pebee.functions.validatePermission(req.user, permissionLabel) || process.env.USE_AUTH === '0') {
                next();
            } else {
                res.status(401).send(pebee.api.responses.notAuthorized());
            }

        } else {
            next();
        }
    };

}


/**
 * Validate user against specific permission
 */
export const checkSpecificPermission = permission => {

    return (req, res, next) => {

        if ( req.method !== 'OPTIONS' ) {

            if ( pebee.functions.validatePermission(req.user, permission) || process.env.USE_AUTH === '0' ) {
                next();
            } else {
                res.status(401).send(pebee.api.responses.notAuthorized());
            }

        } else {
            next();
        }

    }

}