'use strict';


module.exports = config => {

    let expect = config.expect;

    describe('Authentication', () => {

        it('POST /login - should login user and set token', done => {

            config.agent
                .post('/api/login/')
                .send({ username: 'user #1', password: 'password' })
                .end((err, res) => {

                    config.expect(res).to.have.cookie('authorizationToken');

                    done();

                });

        });

        it('PUT /me - should update current user', done => {

            let data = {
                email: 'changed_user@example.com',
                accountCategory: 2
            };

            config.agent
                .put('/api/me')
                .send(data)
                .end((err, res) => {

                    expect(res.body.data).to.be.an('object');
                    expect(res.body.data.email).to.equal(data.email);
                    expect(res.body.data.accountCategory).to.equal(1);

                    done();

                });

        });

        it('PUT /me/change-password - should change current user password', done => {

            let data = {
                password: 'new_password'
            };

            config.agent
                .put('/api/me/change-password')
                .send(data)
                .end((err, res) => {

                    expect(res.body.statusCode).to.equal(200);
                    expect(res.body.message).to.be.a('string');

                    done();

                });

        });

        it('PUT /me/change-password - should change password to previous value', done => {

            let data = {
                password: 'password'
            };

            config.agent
                .put('/api/me/change-password')
                .send(data)
                .end((err, res) => {

                    expect(res.body.statusCode).to.equal(200);
                    expect(res.body.message).to.be.a('string');

                    done();

                });

        });

    });

};

