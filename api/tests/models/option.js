'use strict';

const find = require('underscore').find;


module.exports = config => {

    const agent = config.agent;
    const expect = config.expect;

    describe('Option', () => {

        describe('/api/options', () => {

            it('POST /api/login - should login the user', done => {
    
                agent
                    .post('/api/login/')
                    .send({ username: 'user #1', password: 'password' })
                    .end((err, res) => {
    
                        expect(res).to.have.cookie('authorizationToken');

                        done();
    
                    });
    
            });

            it('GET /all - should return all options', done => {

                agent
                    .get('/api/options/all')
                    .end((err, res) => {

                        expect(res.body.data).to.be.an('array');

                        done();

                    });

            });

            it('GET /:key - should return single option by key', done => {

                agent
                    .get('/api/options/lang')
                    .end((err, res) => {

                        expect(res.body.data).to.be.an('object');
                        expect(res.body.data.key).to.equal('lang');

                        done();

                    });

            });

            it('GET /language - should return current language', done => {

                agent
                    .get('/api/options/language')
                    .end((err, res) => {

                        expect(res.body.data).to.be.an('object');
                        expect(res.body.data).to.have.all.keys('language', 'code');

                        done();

                    });

            });

            it('POST / - should create new option', done => {

                let data = {
                    key: 'api_url',
                    displayName: 'API url',
                    value: 'http://localhost:3000/'
                };

                agent
                    .post('/api/options')
                    .send(data)
                    .end((err, res) => {

                        expect(res.body.data).to.be.an('object');
                        expect(res.body.data).to.own.include(data);

                        done();

                    });

            });

            it('PUT /api_url - should update previously created option', done => {

                let data = {
                    value: 'http://localhost:4000'
                };

                agent
                    .put('/api/options/api_url')
                    .send(data)
                    .end((err, res) => {

                        expect(res.body.data).to.be.an('object');
                        expect(res.body.data).to.own.include(data);

                        done();

                    });
                
            });

            it('PUT /update - should bulk update options', done => {

                let data = {
                    lang: 'pl',
                    api_url: 'https://api.example.com'
                };

                agent
                    .put('/api/options/update')
                    .send(data)
                    .end((err, res) => {

                        expect(res.body.data).to.be.an('array');
                        expect(res.body.data.length).to.equal(2);

                        expect(find(res.body.data, elem => elem.key === 'lang').value).to.equal(data['lang']);
                        expect(find(res.body.data, elem => elem.key === 'api_url').value).to.equal(data['api_url']);

                        done();

                    });

            });

            it('DELETE /:key - should try to delete protected option', done => {

                agent
                    .delete('/api/options/lang')
                    .end((err, res) => {

                        expect(res.body).to.be.an('object');
                        expect(res.body).to.have.all.keys('statusCode', 'message');

                        done();

                    });

            });

        });

    });

};