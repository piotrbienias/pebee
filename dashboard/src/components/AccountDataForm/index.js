import React from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';

// Ant Design
import {
    Form,
    Input,
    Button
} from 'antd';


class AccountDataForm extends React.Component {

    handleSubmit = e => {
        e.preventDefault();

        this.props.form.validateFields(['username', 'email'], (err, values) => {
            if (err) return;

            console.log(values);

            this.props.saveAccount(values);
        });
    }

    render() {
        const { formatMessage } = this.props.intl;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 }
            },
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 }
            }
        };

        return (
            <Form
                onSubmit={this.handleSubmit}>
                <Form.Item
                    label={formatMessage({ id: 'pebee.global.username' })}
                    {...formItemLayout}>
                    {getFieldDecorator('username', {
                        rules: [
                            { required: true, message: formatMessage({ id: 'pebee.account.usernameIsRequired' }) }
                        ],
                        initialValue: this.props.account ? this.props.account.username : ''
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item
                    label={formatMessage({ id: 'pebee.global.email' })}
                    {...formItemLayout}>
                    {getFieldDecorator('email', {
                        rules: [
                            { required: true, message: formatMessage({ id: 'pebee.account.emailIsRequired' }) }
                        ],
                        initialValue: this.props.account ? this.props.account.email : ''
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item
                    label={formatMessage({ id: 'pebee.account.accountCategory' })}
                    {...formItemLayout}>
                    {getFieldDecorator('accountCategory', {
                        initialValue: this.props.account && this.props.account.accountCategory ? this.props.account.accountCategory.name : ''
                    })(
                        <Input disabled={true} />
                    )}
                </Form.Item>
                <Form.Item>
                    <Button
                        style={{ float: 'right' }}
                        type="primary"
                        htmlType="submit"><FormattedMessage id="pebee.global.save" /></Button>
                </Form.Item>
            </Form>
        );
    }

}


export default Form.create()(injectIntl(AccountDataForm));