import React from 'react';
import { injectIntl } from 'react-intl';

// Ant Design
import {
    Tabs,
    Col
} from 'antd';

// Account Data Form
import AccountDataForm from './../AccountDataForm';

// Change Account Password Form
import ChangeAccountPasswordForm from './../ChangeAccountPasswordForm';


class AccountTabs extends React.Component {

    render() {
        const { formatMessage } = this.props.intl;

        return (
            <Col span={16}>
                <Tabs>
                    <Tabs.TabPane tab={formatMessage({ id: 'pebee.account.accountData' })} key="accountData">
                        <AccountDataForm
                            account={this.props.account}
                            saveAccount={this.props.saveAccount} />
                    </Tabs.TabPane>
                    <Tabs.TabPane tab={formatMessage({ id: 'pebee.account.changePassword' })} key="changePassword">
                        <ChangeAccountPasswordForm
                            changePassword={this.props.changePassword} />
                    </Tabs.TabPane>
                </Tabs>
            </Col>
        )
    }

}


export default injectIntl(AccountTabs);