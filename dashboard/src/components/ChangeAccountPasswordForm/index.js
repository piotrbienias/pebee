import React from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';

// Ant Design
import {
    Form,
    Input,
    Button
} from 'antd';


class ChangeAccountPasswordForm extends React.Component {

    handleSubmit = e => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (err) return;

            if ( values['password'] !== values['confirmPassword'] ) {
                let message = this.props.intl.formatMessage({ id: 'pebee.account.passwordAndConfirmationMustMatch' });

                this.props.form.setFields({
                    password: { value: values.password, errors: [new Error(message)] },
                    confirmPassword: { value: values.confirmPassword, errors: [new Error(message)] }
                });

                return;
            }

            this.props.changePassword(values.password);
            this.props.form.resetFields();
        });
    }

    render() {
        const { formatMessage } = this.props.intl;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 }
            },
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 }
            }
        };

        return (
            <Form
                onSubmit={this.handleSubmit}>
                <Form.Item
                    label={formatMessage({ id: 'pebee.global.password' })}
                    {...formItemLayout}>
                    {getFieldDecorator('password', {
                        rules: [
                            { required: true, message: formatMessage({ id: 'pebee.global.passwordIsRequired' }) }
                        ]
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>
                <Form.Item
                    label={formatMessage({ id: 'pebee.global.confirmPassword' })}
                    {...formItemLayout}>
                    {getFieldDecorator('confirmPassword', {
                        rules: [
                            { required: true, message: formatMessage({ id: 'pebee.global.confirmPasswordIsRequired' }) }
                        ]
                    })(
                        <Input type="password" />
                    )}
                </Form.Item>
                <Form.Item>
                    <Button
                        style={{ float: 'right' }}
                        htmlType="submit"
                        type="primary"><FormattedMessage id="pebee.global.save" /></Button>
                </Form.Item>
            </Form>
        );
    }

}


export default Form.create()(injectIntl(ChangeAccountPasswordForm));