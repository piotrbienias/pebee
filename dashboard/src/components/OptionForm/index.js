import React from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';

// Ant Design
import {
    Form,
    Input,
    Select,
    Button
} from 'antd';


class OptionForm extends React.Component {

    handleSubmit = e => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (err) return;

            this.props.saveOption(values);
        });
    }

    render() {
        const { formatMessage } = this.props.intl;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 }
            },
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 }
            }
        };

        return (
            <Form
                onSubmit={this.handleSubmit}>
                <Form.Item>
                    {getFieldDecorator('key', {
                        initialValue: this.props.option ? this.props.option.key : null
                    })(
                        <Input type="hidden" />
                    )}
                </Form.Item>
                <Form.Item
                    label={formatMessage({ id: 'pebee.option.displayName' })}
                    help={formatMessage({ id: 'pebee.option.displayNameTip' })}
                    {...formItemLayout}>
                    {getFieldDecorator('displayName', {
                        rules: [
                            { required: true, message: formatMessage({ id: 'pebee.option.displayNameIsRequired' }) }
                        ],
                        initialValue: this.props.option ? this.props.option.displayName : ''
                    })(
                        <Input />
                    )}
                </Form.Item>
                <Form.Item
                    label={formatMessage({ id: 'pebee.option.availableValues' })}
                    help={formatMessage({ id: 'pebee.option.availableValuesTip' })}
                    {...formItemLayout}>
                    {getFieldDecorator('availableValues', {
                        initialValue: this.props.option ? this.props.option.availableValues : []
                    })(
                        <Select
                            mode="tags"
                            style={{ width: '100%' }} />
                    )}
                </Form.Item>
                <Form.Item>
                    <Button
                        htmlType="submit"
                        type="primary"
                        style={{ float: 'right' }}><FormattedMessage id="pebee.global.save" /></Button>
                </Form.Item>
            </Form>
        )
    }
    
}


export default Form.create()(injectIntl(OptionForm));