import React from 'react';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

// Ant Design
import {
    Menu,
    Icon
} from 'antd';


class TopBar extends React.Component {

    handleClick = item => {
        this.props.history.push(item.key);
    }

    render() {
        return (
            <Menu
                onClick={this.handleClick}
                mode="horizontal"
                style={{ textAlign: 'right' }}>
                <Menu.Item key="/account">
                    <Icon type="setting" /><FormattedMessage id="pebee.global.yourAccount" />
                </Menu.Item>
            </Menu>
        )
    }

}

TopBar.contextTypes = {
    router: PropTypes.object.isRequired
};


export default withRouter(TopBar);