/**
 * Account actions
 */


import {
    SAVE_ACCOUNT,
    SAVE_ACCOUNT_FAILURE,
    SAVE_ACCOUNT_SUCCESS,

    CHANGE_PASSWORD,
    CHANGE_PASSWORD_FAILURE,
    CHANGE_PASSWORD_SUCCESS
} from './constants';


/* Save Account */
export const saveAccount = data => {
    return {
        type: SAVE_ACCOUNT,
        data
    }
};

export const saveAccountSuccess = data => {
    return {
        type: SAVE_ACCOUNT_SUCCESS,
        data
    }
};

export const saveAccountFailure = data => {
    return {
        type: SAVE_ACCOUNT_FAILURE,
        data
    }
};


/* Change Password */
export const changePassword = data => {
    return {
        type: CHANGE_PASSWORD,
        data
    }
};

export const changePasswordSuccess = data => {
    return {
        type: CHANGE_PASSWORD_SUCCESS,
        data
    }
};

export const changePasswordFailure = data => {
    return {
        type: CHANGE_PASSWORD_FAILURE,
        data
    }
};