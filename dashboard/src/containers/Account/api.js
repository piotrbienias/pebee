/**
 * Account API
 */

import axios from './../../utils/axios';


export const saveAccount = data => {
    return axios.put('/me', data);
};

export const changePassword = password => {
    return axios.put('/me/change-password', { password });
};