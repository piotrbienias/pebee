/**
 * Account constants
 */

export const SAVE_ACCOUNT = 'PeBee/Account/SaveAccount';
export const SAVE_ACCOUNT_SUCCESS = 'PeBee/Account/SaveAccountSuccess';
export const SAVE_ACCOUNT_FAILURE = 'PeBee/Account/SaveAccountFailure';

export const CHANGE_PASSWORD = 'PeBee/Account/ChangePassword';
export const CHANGE_PASSWORD_SUCCESS = 'PeBee/Account/ChangePasswordSuccess';
export const CHANGE_PASSWORD_FAILURE = 'PeBee/Account/ChangePasswordFailure';