import React from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import withReducer from './../../utils/withReducer';
import withSaga from './../../utils/withSaga';

// Ant Design
import {
    message
} from 'antd';

// Account Tabs
import AccountTabs from './../../components/AccountTabs';

// Actions
import { saveAccount, changePassword } from './actions';

// Reducer
import reducer from './reducer';

// Selector
import selector from './selector';

// Saga
import saga from './saga';


class Account extends React.Component {

    componentDidUpdate(prevProps, prevState) {
        if (this.props.message && this.props.message !== prevProps.message && this.props.messageType !== '') {
            message[this.props.messageType](this.props.message);
        }
    }

    render() {
        return (
            <AccountTabs
                account={this.props.account}
                saveAccount={this.props.saveAccount}
                changePassword={this.props.changePassword} />
        )
    }

}


Account.propTypes = {
    account: PropTypes.object.isRequired,
    saveAccount: PropTypes.func.isRequired,
    message: PropTypes.string,
    messageType: PropTypes.string.isRequired,

    changePassword: PropTypes.func.isRequired
};


const mapStateToProps = state => {
    return selector(state);
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            saveAccount,
            changePassword
        },
        dispatch
    );
}

const injectReducer = withReducer('account', reducer);
const injectConnect = connect(mapStateToProps, mapDispatchToProps);
const injectSaga = withSaga('account', saga);


export default compose(
    injectReducer,
    injectSaga,
    injectConnect
)(Account);