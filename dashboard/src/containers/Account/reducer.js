/**
 * Account reducer
 */

import { fromJS, Map } from 'immutable';

import {
    SAVE_ACCOUNT_SUCCESS,
    SAVE_ACCOUNT_FAILURE,

    CHANGE_PASSWORD_FAILURE,
    CHANGE_PASSWORD_SUCCESS
} from './constants';


const initialState = fromJS({
    account: JSON.parse(localStorage.getItem('account')),
    message: null,
    messageType: 'success'
});


function accountReducer(state = initialState, action) {

    switch (action.type) {
        case SAVE_ACCOUNT_SUCCESS:
            return state
                .set('account', Map(action.data.data))
                .set('messageType', 'success')
                .set('message', action.data.message);

        case SAVE_ACCOUNT_FAILURE:
            return state
                .set('messageType', 'error')
                .set('message', action.data);

        case CHANGE_PASSWORD_SUCCESS:
            return state
                .set('messageType', 'success')
                .set('message', action.data);

        case CHANGE_PASSWORD_FAILURE:
            return state
                .set('messageType', 'error')
                .set('message', action.data);

        default:
            return state
                .set('message', null);

    }

}


export default accountReducer;