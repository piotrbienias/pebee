/**
 * Account saga
 */

import { takeLatest, put } from 'redux-saga/effects';
import { saveAccountFailure, saveAccountSuccess, changePasswordFailure, changePasswordSuccess } from './actions';
import { SAVE_ACCOUNT, CHANGE_PASSWORD } from './constants';
import { saveAccount, changePassword } from './api';


function* saveAccountSaga(action) {

    try {
        const response = yield saveAccount(action.data);

        yield put(saveAccountSuccess(response.data));
    } catch (e) {
        yield put(saveAccountFailure(e.response.data.message));
    }

}

function* changePasswordSaga(action) {

    try {
        const response = yield changePassword(action.data);

        yield put(changePasswordSuccess(response.data.message));
    } catch (e) {
        yield put(changePasswordFailure(e.response.data.message));
    }

}


function* accountSaga() {
    yield takeLatest(SAVE_ACCOUNT, saveAccountSaga);
    yield takeLatest(CHANGE_PASSWORD, changePasswordSaga);
}


export default accountSaga;