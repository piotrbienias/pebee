/**
 * Account selector
 */

import { createSelector } from 'reselect';


const getAccountState = state => state.account;


const accountSelector = createSelector(
    [ getAccountState ],
    ( accountState ) => {
        return accountState.toJS();
    }
)


export default accountSelector;