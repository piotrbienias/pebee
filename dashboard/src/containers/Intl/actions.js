/**
 * Intl actions
 */


import {
    FETCH_LANGUAGE,
    FETCH_LANGUAGE_FAILURE,
    FETCH_LANGUAGE_SUCCESS
} from './constants';


/* Fetch language */
export const fetchLanguage = () => {
    return {
        type: FETCH_LANGUAGE
    }
};

export const fetchLanguageSuccess = data => {
    return {
        type: FETCH_LANGUAGE_SUCCESS,
        data
    }
};

export const fetchLanguageFailure = data => {
    return {
        type: FETCH_LANGUAGE_FAILURE,
        data
    }
};