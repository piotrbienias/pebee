import axios from './../../utils/axios';


export const getLanguage = () => {
    return axios.get('/options/language');
};