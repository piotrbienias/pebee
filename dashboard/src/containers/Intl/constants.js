/**
 * Intl constants
 */


export const FETCH_LANGUAGE = 'PeBee/Intl/FetchLanguage';
export const FETCH_LANGUAGE_SUCCESS = 'PeBee/Intl/FetchLanguageSuccess';
export const FETCH_LANGUAGE_FAILURE = 'PeBee/Intl/FetchLanguageFailure';