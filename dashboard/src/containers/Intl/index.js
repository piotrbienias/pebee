import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { compose, bindActionCreators } from 'redux';

import withReducer from './../../utils/withReducer';
import withSaga from './../../utils/withSaga';

// Intl
import { IntlProvider } from 'react-intl';

// Actions
import { fetchLanguage } from './actions';

// Selector
import selector from './selector';

// Reducer
import reducer from './reducer';

// Saga
import saga from './saga';

import intlMessages from './../../utils/intl';



class Intl extends React.Component {

    componentDidMount() {
        this.props.fetchLanguage();
    }

    render() {
        const messages = intlMessages[this.props.locale] ? intlMessages[this.props.locale] : {};

        return (
            <IntlProvider messages={messages} locale={this.props.locale}>
                {this.props.children}
            </IntlProvider>
        )
    }

}


Intl.propTypes = {
    children: PropTypes.object.isRequired,
    locale: PropTypes.string.isRequired,
    fetchLanguage: PropTypes.func.isRequired
};


const mapStateToProps = state => {
    return selector(state);
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            fetchLanguage
        },
        dispatch
    )
};

const injectReducer = withReducer('intl', reducer);
const injectSaga = withSaga('intl', saga);
const injectConnect = connect(mapStateToProps, mapDispatchToProps);



export default compose(
    injectReducer,
    injectSaga,
    injectConnect
)(Intl);