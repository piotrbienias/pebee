/**
 * Intl reducer
 */

import { fromJS } from 'immutable';
import { FETCH_LANGUAGE_FAILURE, FETCH_LANGUAGE_SUCCESS } from './constants';
import { SAVE_OPTIONS_SUCCESS } from './../Options/constants';


const initialState = fromJS({
    locale: localStorage.getItem('pebee-locale') || 'en'
});


function intlReducer(state = initialState, action) {

    switch(action.type) {
        case FETCH_LANGUAGE_SUCCESS:
            localStorage.setItem('pebee-locale', action.data);

            return state
                .set('locale', action.data);

        case FETCH_LANGUAGE_FAILURE:
            return state
                .set('locale', 'en');

        case SAVE_OPTIONS_SUCCESS:
            let localeOption = action.data.find(option => {
                return option.key === 'lang';
            });

            localStorage.setItem('pebee-locale', localeOption.value);
            return state
                .set('locale', localeOption.value);
        
        default:
            return state;
    }

}


export default intlReducer