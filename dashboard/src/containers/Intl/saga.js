/**
 * Intl saga
 */

import { takeLatest, put } from 'redux-saga/effects';
import { fetchLanguageFailure, fetchLanguageSuccess } from './actions';
import { FETCH_LANGUAGE } from './constants';
import { getLanguage } from './api';


function* fetchLanguageSaga() {

    try {
        const response = yield getLanguage();

        yield put(fetchLanguageSuccess(response.data.data.code));
    } catch (e) {
        yield put(fetchLanguageFailure(e.response.data.message));
    }

}


function* intlSaga() {
    yield takeLatest(FETCH_LANGUAGE, fetchLanguageSaga);
}


export default intlSaga;