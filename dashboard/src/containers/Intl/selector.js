import { createSelector } from 'reselect';


const selectIntl = state => state.intl;


const selectLocale = createSelector(
    [ selectIntl ],
    (intlState) => {
        return intlState.toJS();
    }
);


export default selectLocale;