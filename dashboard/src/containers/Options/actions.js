/**
 * Options actions
 */


import {
    FETCH_OPTIONS,
    FETCH_OPTIONS_FAILURE,
    FETCH_OPTIONS_SUCCESS,
    
    SAVE_OPTIONS,
    SAVE_OPTIONS_SUCCESS,
    SAVE_OPTIONS_FAILURE
} from './constants';


/* Fetch available options */
export const fetchOptions = () => {
    return {
        type: FETCH_OPTIONS
    }
};

export const fetchOptionsSuccess = data => {
    return {
        type: FETCH_OPTIONS_SUCCESS,
        data
    }
};

export const fetchOptionsFailure = data => {
    return {
        type: FETCH_OPTIONS_FAILURE,
        data
    }
};


/* Save options */
export const saveOptions = data => {
    return {
        type: SAVE_OPTIONS,
        data
    }
};

export const saveOptionsSuccess = data => {
    return {
        type: SAVE_OPTIONS_SUCCESS,
        data
    }
};

export const saveOptionsFailure = data => {
    return {
        type: SAVE_OPTIONS_FAILURE,
        data
    }
};