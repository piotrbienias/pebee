import axios from './../../utils/axios';


export const getAvailableOptions = () => {
    return axios.get('/options/all');
};

export const saveOptions = data => {
    return axios.put('/options/update', data);
};