/**
 * Options constants
 */


export const FETCH_OPTIONS = 'PeBee/Options/FetchOptions';
export const FETCH_OPTIONS_SUCCESS = 'PeBee/Options/FetchOptionsSuccess';
export const FETCH_OPTIONS_FAILURE = 'PeBee/Options/FetchOptionsFailure';

export const SAVE_OPTIONS = 'PeBee/Options/SaveOptions';
export const SAVE_OPTIONS_SUCCESS = 'PeBee/Options/SaveOptionsSuccess';
export const SAVE_OPTIONS_FAILURE = 'PeBee/Options/SaveOptions/Failure';