/**
 * Options reducer
 */


import { fromJS, List } from 'immutable';

import {
    FETCH_OPTIONS_FAILURE,
    FETCH_OPTIONS_SUCCESS,
    SAVE_OPTIONS_SUCCESS,
    SAVE_OPTIONS_FAILURE
} from './constants';


const initialState = fromJS({
    options: [],
    message: null,
    messageType: 'success'
});


function optionsReducer(state = initialState, action) {

    switch (action.type) {

        case FETCH_OPTIONS_SUCCESS:
            return state
                .set('options', List(action.data))
                .set('message', null);

        case FETCH_OPTIONS_FAILURE:
            return state
                .set('message', action.data)
                .set('messageType', 'error');

        case SAVE_OPTIONS_SUCCESS:
            return state
                .set('options', List(action.data))
                .set('message', 'pebee.options.saveSuccess')
                .set('messageType', 'success');

        case SAVE_OPTIONS_FAILURE:
            return state
                .set('options', initialState.get('options'))
                .set('message', action.data)
                .set('messageType', 'error');

        default:
            return state;

    }

}


export default optionsReducer;