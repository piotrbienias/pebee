import { takeLatest, put } from 'redux-saga/effects';
import { getAvailableOptions, saveOptions } from './api';
import { fetchOptionsFailure, fetchOptionsSuccess, saveOptionsFailure, saveOptionsSuccess } from './actions';
import { FETCH_OPTIONS, SAVE_OPTIONS } from './constants';


function* fetchAvailableOptionsSaga() {

    try {
        const response = yield getAvailableOptions();

        yield put(fetchOptionsSuccess(response.data.data));
    } catch (e) {
        yield put(fetchOptionsFailure(e.response.data.message));
    }

}


function* saveOptionsSaga(action) {

    try {
        const response = yield saveOptions(action.data);

        yield put(saveOptionsSuccess(response.data.data));
    } catch (e) {
        yield put(fetchOptionsFailure(e.response.data.message));
    }

}


function* optionsSaga() {
    yield takeLatest(FETCH_OPTIONS, fetchAvailableOptionsSaga);
    yield takeLatest(SAVE_OPTIONS, saveOptionsSaga);
}


export default optionsSaga;