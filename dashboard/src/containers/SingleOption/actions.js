/**
 * Single option actions
 */

import {
    SAVE_OPTION,
    SAVE_OPTION_FAILURE,
    SAVE_OPTION_SUCCESS,

    FETCH_OPTION,
    FETCH_OPTION_FAILURE,
    FETCH_OPTION_SUCCESS,

    RESET_OPTION
} from './constants';


/* Save Option */
export const saveOption = data => {
    return {
        type: SAVE_OPTION,
        data
    }
};

export const saveOptionSuccess = data => {
    return {
        type: SAVE_OPTION_SUCCESS,
        data
    }
};

export const saveOptionFailure = data => {
    return {
        type: SAVE_OPTION_FAILURE,
        data
    }
};


/* Fetch Option */
export const fetchOption = data => {
    return {
        type: FETCH_OPTION,
        data
    }
};

export const fetchOptionSuccess = data => {
    return {
        type: FETCH_OPTION_SUCCESS,
        data
    }
};

export const fetchOptionFailure = data => {
    return {
        type: FETCH_OPTION_FAILURE,
        data
    }
};


/* Reset Option */
export const resetOption = () => {
    return {
        type: RESET_OPTION
    }
};