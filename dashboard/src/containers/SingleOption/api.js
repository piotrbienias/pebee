/**
 * Single option API
 */

import axios from './../../utils/axios';


export const saveOption = data => {
    if ( data.key ) {
        return axios.put(`/options/${data.key}`, data);
    } else {
        return axios.post('/options', data);
    }
};

export const fetchOption = key => {
    return axios.get(`/options/${key}`);
};