/**
 * Single option constants
 */

export const SAVE_OPTION = 'PeBee/SingleOption/SaveOption';
export const SAVE_OPTION_SUCCESS = 'PeBee/SingleOption/SaveOptionSuccess';
export const SAVE_OPTION_FAILURE = 'PeBee/SingleOption/SaveOptionFailure';

export const FETCH_OPTION = 'PeBee/SingleOption/FetchOption';
export const FETCH_OPTION_SUCCESS = 'PeBee/SingleOption/FetchOptionSuccess';
export const FETCH_OPTION_FAILURE = 'PeBee/SingleOption/FetchOptionFailure';

export const RESET_OPTION = 'PeBee/SingleOption/ResetOption';