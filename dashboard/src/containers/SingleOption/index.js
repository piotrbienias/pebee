import React from 'react';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';

import withReducer from './../../utils/withReducer';
import withSaga from './../../utils/withSaga';

// Ant Design
import {
    Col,
    message
} from 'antd';

// Option Form
import OptionForm from './../../components/OptionForm';

// Actions
import { saveOption, fetchOption, resetOption } from './actions';

// Saga
import saga from './saga';

// Reducer
import reducer from './reducer';

// Selector
import selector from './selector';



class SingleOption extends React.Component {

    componentDidMount() {
        let key = this.props.match.params['key'];

        if (key && key !== '') {
            this.props.fetchOption(key);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if ( this.props.message && this.props.message !== prevProps.message && this.props.messageType !== '' ) {
            message[this.props.messageType](this.props.message);
        }
    }

    render() {
        return (
            <div>
                <Col span={16}>
                    <h1>
                        <FormattedMessage id="pebee.options.systemOptions" />
                    </h1>
                    <OptionForm
                        saveOption={this.props.saveOption}
                        option={this.props.option} />
                </Col>
            </div>
        )
    }

    componentWillUnmount() {
        this.props.resetOption();
    }

}

SingleOption.contextTypes = {
    router: PropTypes.object.isRequired
};

SingleOption.propTypes = {
    option: PropTypes.object.isRequired,
    message: PropTypes.string,
    messageType: PropTypes.string.isRequired,
    saveOption: PropTypes.func.isRequired,
    fetchOption: PropTypes.func.isRequired,
    resetOption: PropTypes.func.isRequired
};

const mapStateToProps = state => {
    return selector(state);
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        {
            saveOption,
            fetchOption,
            resetOption
        },
        dispatch
    );
};

const injectReducer = withReducer('singleOption', reducer);
const injectSaga = withSaga('singleOption', saga);
const injectConnect = connect(mapStateToProps, mapDispatchToProps);


export default compose(
    injectReducer,
    injectSaga,
    injectConnect
)(SingleOption);