/**
 * Single option reducer
 */


import { fromJS, Map } from 'immutable';

import {
    SAVE_OPTION_FAILURE,
    SAVE_OPTION_SUCCESS,

    FETCH_OPTION_FAILURE,
    FETCH_OPTION_SUCCESS,

    RESET_OPTION
} from './constants';


const initialState = fromJS({
    option: {},
    message: null,
    messageType: 'success'
});


function singleOptionReducer(state = initialState, action) {

    switch (action.type) {

        case SAVE_OPTION_SUCCESS:
            return state
                .set('option', Map(action.data.data))
                .set('message', action.data.message)
                .set('messageType', 'success');

        case SAVE_OPTION_FAILURE:
            return state
                .set('message', action.data)
                .set('messageType', 'error');

        case FETCH_OPTION_SUCCESS:
            return state
                .set('option', Map(action.data));

        case FETCH_OPTION_FAILURE:
            return state
                .set('option', Map({}))
                .set('message', action.data)
                .set('messageType', 'error');

        case RESET_OPTION:
            return state
                .set('option', Map({}));

        default:
            return state;

    }

}


export default singleOptionReducer;