/**
 * Single option saga
 */

import { takeLatest, put } from 'redux-saga/effects';
import { saveOptionFailure, saveOptionSuccess, fetchOptionFailure, fetchOptionSuccess} from './actions';
import { SAVE_OPTION, FETCH_OPTION } from './constants';
import { saveOption, fetchOption } from './api';


function* saveOptionSaga(action) {

    try {
        const response = yield saveOption(action.data);

        yield put(saveOptionSuccess(response.data));
    } catch (e) {
        yield put(saveOptionFailure(e.response.data.message));
    }

}

function* fetchOptionSaga(action) {

    try {
        const response = yield fetchOption(action.data);

        yield put(fetchOptionSuccess(response.data.data));
    } catch (e) {
        yield put(fetchOptionFailure(e.response.data.message));
    }

}


function* singleOptionSaga() {
    yield takeLatest(SAVE_OPTION, saveOptionSaga);
    yield takeLatest(FETCH_OPTION, fetchOptionSaga);
}


export default singleOptionSaga;