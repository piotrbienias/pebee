/**
 * Single option selector
 */

import { createSelector } from 'reselect';


const getSingleOptionState = state => state.singleOption;


const singleOptionSelector = createSelector(
    [ getSingleOptionState ],
    ( singleOptionState ) => {
        return singleOptionState.toJS();
    }
);


export default singleOptionSelector;