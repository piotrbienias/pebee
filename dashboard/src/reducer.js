import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';


const createReducer = asyncReducers => {
    return combineReducers({
        router: routerReducer,
        ...asyncReducers
    });
}


export default createReducer;