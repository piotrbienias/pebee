import React from 'react';
import PropTypes from 'prop-types';


const withReducer = (key, reducer) => WrappedComponent => {

    class WithReducer extends React.Component {

        componentWillMount() {
            this.context.store.injectReducer(key, reducer);
        }

        render() {
            return <WrappedComponent {...this.props} />
        }

    }

    WithReducer.contextTypes = {
        store: PropTypes.object.isRequired
    };

    return WithReducer;

}


export default withReducer;