import React from 'react';
import PropTypes from 'prop-types';


const withSaga = (key, saga) => WrappedComponent => {

    class WithSaga extends React.Component {

        componentWillMount() {
            this.context.store.injectSaga(key, saga);
        }

        render() {
            return <WrappedComponent {...this.props} />
        }

    }

    WithSaga.contextTypes = {
        store: PropTypes.object
    };

    return WithSaga;
    
}


export default withSaga;